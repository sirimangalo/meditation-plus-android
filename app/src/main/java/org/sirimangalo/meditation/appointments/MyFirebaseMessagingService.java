/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sirimangalo.meditation.appointments;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static androidx.core.app.NotificationCompat.PRIORITY_MAX;

/**
 * NOTE: There can only be one service in each app that receives FCM messages. If multiple
 * are declared in the Manifest then the first one will be chosen.
 *
 * In order to make this Java sample functional, you must remove the following from the Kotlin messaging
 * service in the AndroidManifest.xml:
 *
 * <intent-filter>
 *   <action android:name="com.google.firebase.MESSAGING_EVENT" />
 * </intent-filter>
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and org.sirimangalo.meditation.org.sirimangalo.meditation.notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // org.sirimangalo.meditation.org.sirimangalo.meditation.notification is displayed.
        // When the user taps on the org.sirimangalo.meditation.org.sirimangalo.meditation.notification they are returned to the app. Messages
        // containing both org.sirimangalo.meditation.org.sirimangalo.meditation.notification
        // and data payloads are treated as org.sirimangalo.meditation.org.sirimangalo.meditation.notification messages. The Firebase console always
        // sends org.sirimangalo.meditation.org.sirimangalo.meditation.notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            String msgBody = "";
            if (remoteMessage.getData().containsKey("body")) {
                msgBody = remoteMessage.getData().get("body");
            }

            sendNotification(msgBody);
        }
    }
    // [END receive_message]


    // [START on_new_token]
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     * @Override
     *     public void onNewToken(String token) {
     *         Log.d(TAG, "Refreshed token: " + token);
     *
     *         // If you want to send messages to this application instance or
     *         // manage this apps subscriptions on the server side, send the
     *         // Instance ID token to your app server.
     *         sendRegistrationToServer(token);
     *     }
     */
    // [END on_new_token]


    /**
     * Create and show a simple org.sirimangalo.meditation.org.sirimangalo.meditation.notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        // the url should be replaced with the permanent discord invite link
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://meditation.sirimangalo.org"));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

        long[] vibrationPattern = {1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000};

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_stat_phone)
                        .setContentTitle("Appointment CALL")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setVibrate(vibrationPattern)
                        .setPriority(PRIORITY_MAX)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Basically the same as in MainActivity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // set id, name, priority
            CharSequence name = getString(R.string.default_notification_channel_name);
            String description = getString(R.string.default_notification_channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);

            // set description
            channel.setDescription(description);

            // set ringtone
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                    .build();
            channel.setSound(defaultSoundUri, attributes);

            // set vibration
            channel.enableVibration(true);
            channel.setVibrationPattern(vibrationPattern);

            // set lights
            channel.enableLights(true);
            channel.setLightColor(R.color.colorPrimary);

            // create channel
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(1337, notificationBuilder.build());
    }
}
