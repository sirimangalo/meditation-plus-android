package org.sirimangalo.meditation.appointments;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.messaging.FirebaseMessaging;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // create notification channel if >= Android O
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // set id, name, priority
            String channelId = getString(R.string.default_notification_channel_id);
            CharSequence name = getString(R.string.default_notification_channel_name);
            String description = getString(R.string.default_notification_channel_description);
            NotificationChannel channel = new NotificationChannel(channelId, name, NotificationManager.IMPORTANCE_HIGH);

            // set description
            channel.setDescription(description);

            // set ringtone
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                    .build();
            channel.setSound(defaultSoundUri, attributes);

            // set vibration
            long[] vibrationPattern = {1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000, 800, 1000};
            channel.enableVibration(true);
            channel.setVibrationPattern(vibrationPattern);

            // set lights
            channel.enableLights(true);
            channel.setLightColor(Color.GREEN);

            // create channel
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            // show debugging information in app
            if (notificationManager.getNotificationChannel(channelId) != null) {
                Log.d(TAG, "Debugging Channel");
                NotificationChannel checkChannel = notificationManager.getNotificationChannel(channelId);
                TextView textViewDebug = (TextView)findViewById(R.id.textView);
                TextView textViewDebugRingtone = (TextView)findViewById(R.id.textViewDebugRingtone);
                TextView textViewDebugPriority = (TextView)findViewById(R.id.textViewDebugPriority);
                TextView textViewDebugVibration = (TextView)findViewById(R.id.textViewDebugVibration);
                TextView textViewDebugLights = (TextView)findViewById(R.id.textViewDebugLights);

                textViewDebug.setVisibility(View.VISIBLE);
                textViewDebugRingtone.setText("RINGTONE: " + checkChannel.getSound().toString());
                textViewDebugPriority.setText("IMPORTANCE: " + checkChannel.getImportance());
                textViewDebugVibration.setText("VIBRATION: " + Arrays.equals(checkChannel.getVibrationPattern(), vibrationPattern));
                textViewDebugLights.setText("LIGHTS: " + checkChannel.getLightColor());
            }
        }

        // Handle possible data accompanying notification message.
        // [START handle_data_extras]
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }
        // [END handle_data_extras]

        Button subscribeButton = findViewById(R.id.subscribeButton);
        subscribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Subscribing to appointment topic");
                // [START subscribe_topics]
                FirebaseMessaging.getInstance().subscribeToTopic("appointments-staging")
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                String msg = getString(R.string.msg_subscribed);
                                if (!task.isSuccessful()) {
                                    msg = getString(R.string.msg_subscribe_failed);
                                }
                                Log.d(TAG, msg);
                                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        });
                // [END subscribe_topics]
            }
        });
    }

}
