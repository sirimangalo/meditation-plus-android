# INACTIV PROJECT

# meditation-plus-notifications

A minimalistic app which subscribes to a certain messaging topic in order to receive push notifications with a ringing tone.

## Installation

### prepare

- Unknown Sources should be allowed for installing the APK file:
    - See https://developer.android.com/distribute/marketing-tools/alternative-distribution#unknown-sources

Note: If installation fails it might also be because of activated "Google Play Protect"

### download

APKs can be found in [artifacts of CI jobs](https://gitlab.com/sirimangalo/meditation-plus-android/-/jobs):

- Name: "build-production" => for exclusive use on meditation.sirimangalo.org
- Name: "build-staging" => for testing purposes on meditation-dev.sirimangalo.org

### activate

... by clicking on the "subscribe" button. 

## Builds

APKs are build with Gitlab CI and can be downloaded as (private) artifacts:

- Each commit triggers a build of the staging apk versioned with the associated pipeline id
    - subscribes to topic: `appointments-staging` 
- Each tag triggers a build of the production apk versioned with the tag 
    - subscribes to topic: `appointments-<< insert token >>`
    - NOTE: For adding new TAGs please only name the tags in this format: MAJOR.MINOR (i.e. "1.0")

## dev notes

### Example of sending manual message

```javascript
// yarn add firebase-admin
const admin = require('firebase-admin');
const serviceAccount = require('./myconfig.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://myproject.firebaseio.com'
});

const message = {
  notification: { 
    title: 'Testing', 
    body: 'Message sent at' + new Date().toString(),
  },
  android:{
    priority: 'high',
  },
  topic: 'appointments-staging'
};

// Send a message to devices subscribed to the provided topic.
admin.messaging().send(message)
  .then((response) => {
    // Response is a message ID string.
    console.log('Successfully sent message:', response);
  })
  .catch((error) => {
    console.log('Error sending message:', error);
  });
```

### High priority message payload

The goal is to make the message as much _high priority_ as possible. Apart from specifying `priority: 'high'` in the message payload it seemed that better results also came by **using notification messages instead of data messages**. The reason for this might be, that the handling for data messages happens in the app whereas notification messages seem to be processed by some system service. For tests where the phone was asleep, data messages were delayed or did not show up at all. The drawback of this is that custom handling is limited, but still possible with Android notification channel (to set sound, light, etc.).

### Android notification channel

For using a custom notification sound, audio stream etc. a new notification channel is created.
